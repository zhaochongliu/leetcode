//////////////////////////////////Qustion///////////////////////////////////
/*Given two arrays, write a function to compute their intersection.
 *Example:
 *Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2, 2]
 */

/////////////////////////////////Solution///////////////////////////////////
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class Solution{
    public int[] intersect (int[] num1, int[] num2) {
        Integer[] newnum1 = new Integer[num1.length];
        for (int i = 0; i < num1.length; i++) newnum1[i] = Integer.valueOf(num1[i]);
        List<Integer> list1 = new ArrayList<Integer>(Arrays.asList(newnum1));
        Integer[] newnum2 = new Integer[num2.length];
        for (int i = 0; i < num2.length; i++) newnum2[i] = Integer.valueOf(num2[i]);
        List<Integer> list2 = new ArrayList<Integer>(Arrays.asList(newnum2));
        List<Integer> list3 = new ArrayList<Integer>();

        for (Integer a: list1){
            for (int i = 0; i < list2.size(); i++) {
                if (a.equals(list2.get(i))) {
                    list3.add(a);
                    list2.remove(a);
                    break;
                }
            }
        }

        int[] x = new int[list3.size()];
        for (int i = 0; i < list3.size(); i++) x[i] = list3.get(i);

        return x;
    }

    public static void main (String[] args) {
        Solution x = new Solution();
        int[] a = {1, 2, 2, 1};
        int[] b = {2, 2};
        int[] c = x.intersect(a, b);

        for (int term : c) System.out.println(term);
    }
}
