class VP {
    public boolean validPalindrome (String a) {
        int end = a.length() - 1;
	for (int pre = 0; pre <= end; pre++, end--) {
            if (a.charAt(pre) != a.charAt(end)) return false;
        }
        return true;
    }

// consider only alphanumeric characters, case non-sensitive.
    public boolean isPalindrome (String a) {
        int pre = 0;
        int end = a.length() - 1;
        while(pre < end) {
            while (pre < end && !Character.isLetterOrDigit(a.charAt(pre)))    pre++;
            while (pre < end && !Character.isLetterOrDigit(a.charAt(end)))    end--;
            if(Character.toLowerCase(a.charAt(pre))
                 != Character.toLowerCase(a.charAt(end)))    return false;
            pre++; 
            end--;
        }
        return true;
    }
    public static void main(String[] args) {
        String a = "122?1";
        VP vp = new VP();
        System.out.println(vp.validPalindrome(a));
        System.out.println(vp.isPalindrome(a));
    }
}
