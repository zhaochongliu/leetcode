class TwoSumHard {

    private Map<Integer, Integer> map = new HashMap<>();

    public void add(int input) {
        int count = map.containsKey(input) ? count : 0;
        map.put(input, count + 1); 
    }
    public boolean find(int n) {
        for(Map.Entry<> entry: map.entrySet()) {
            int key    = entry.getKey();
            int target = n - key;
            if (target = key){
                if (map.get(target) > 1) return true;
            } else if (map.containsKey(target)) return true;
           
        }
        return false;
    }
}
