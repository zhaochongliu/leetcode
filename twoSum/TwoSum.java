import java.util.Map;
import java.util.HashMap;

class TwoSum
{
    private int a[] = {1, 2, 3, 4, 5};
    private int x   = 8;

//basic two sum function
    public int[] twoSum (int[] numbers, int target) {
        Map <Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < numbers.length; i++) {
            int n = numbers[i];
            if (map.containsKey(target - n)) {
                return new int[] {map.get(target - n) + 1, i + 1};
            }
            map.put(n, i);
        }
        throw new IllegalArgumentException("no result");
    }

// binary search in a sorted list
    public int bSearch (int[] numbers, int key, int start) {
        int l = start;
        int r = numbers.length - 1;
        while (l < r) {
            int m = (l + r) / 2;
            if (numbers[m] < key) l = m + 1;
            else r = m;
        }
        return (l == r && numbers[l] == key) ? l : -1;
    }

    public int[] twoSum2(int[] numbers, int target) {
        for (int i = 0; i < numbers.length; i++) {
            int j = bSearch(numbers, target - numbers[i], i + 1);
            if (j != -1) return new int[] {i + 1, j + 1};
        }
        throw new IllegalArgumentException("no result");
    }

// two sum in sorted list easiest way
    public int[] twoSum3 (int[] numbers, int target) {
        int l   = 0;
        int r   = numbers.length - 1;
        while( l < r) {
            int sum = numbers[l] + numbers[r];
            if (sum > target) r--;
            else if (sum < target) l++;
            else return new int[] {l + 1, r + 1};
        }
        throw new IllegalArgumentException("no result");
    }

// testing function
    public static void main(String[] args) {
        TwoSum test = new TwoSum();
        int[] a = test.twoSum3(test.a, test.x);
        System.out.println(a[0] + ", " + a[1]);    

    }
}
