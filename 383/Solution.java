////////////////////////////////////Qestion//////////////////////////////////
/* Given  an  arbitrary  ransom  note  string  and  another  string  containing  letters from  all  the  magazines,  
 * write  a  function  that  will  return  true  if  the  ransom   note  can  be  constructed  from  the  magazines ;  
 * otherwise,  it  will  return  false.   
 * Each  letter  in  the  magazine  string  can  only  be  used  once  in  your  ransom  note.
 * Note: You may assume that both strings contain only lowercase letters.
 * Example: 
 *     canConstruct("a", "b") -> false
 *     canConstruct("aa", "ab") -> false
 *     canConstruct("aa", "aab") -> true
 */
////////////////////////////////////Solution//////////////////////////////////

class Solution {
	public boolean canConstruct(String ransomNote, String magazine)
	{
		if (ransomNote.length() > magazine.length()) return false;
		else 
		{
			int [] alphabet = new int [26];
			for (int c : ransomNote.toCharArray())    alphabet[c - 97] --;
			for (int cc : magazine.toCharArray())    alphabet[cc - 97] ++;
			for (int a : alphabet) if (a < 0) return false;
		}
		return true;
	}

	static public void main (String args[])
	{
		Solution q= new Solution();
		System.out.println(q.canConstruct("a", "b"));
		Solution q2= new Solution();
		System.out.println(q2.canConstruct("aa", "ab"));
		Solution q3= new Solution();
		System.out.println(q3.canConstruct("aa", "aab"));
		Solution q4= new Solution();
		System.out.println(q4.canConstruct("abc", "bac"));
	}
}