//////////////////////////////////Qustion///////////////////////////////////
/* Given two arrays, write a function to compute their intersection.
 * Example:
 * Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2].
 * Note:
 * Each element in the result must be unique.
 * The result can be in any order.
 */

/////////////////////////////////Solution///////////////////////////////////
import java.util.List;
import java.util.ArrayList;

public class Solution {
    public int[] intersection(int[] nums1, int[] nums2) {
        List<Integer> result = new ArrayList<Integer> ();
        for (int a : nums1) {
            for (int b : nums2) {
                if (a == b && !result.contains(a)) 
                result.add(a);
            }
        }
        int[] x = new int[result.size()];
        for(int i = 0; i < result.size(); i++) x[i] = result.get(i);
        return x;
    }
    
    public static void main(String[] args){
        int[] a = {1, 2, 3, 4, 4, 5};
        int[] b = {3, 6};
        Solution x = new Solution();
        int[] result = x.intersection(a, b);
        for (int term : result) System.out.println(term);
    }
}
