class StrStr {
    public int strstr (String needle, String haystack) {
        for (int i = 0; ; i++) {
            for (int j = 0; ; j++) {
                if (j == needle.length()) return i;
                if (i + j == haystack.length()) return -1;
                if (needle.charAt(j) != haystack.charAt(i + j)) break;
            }
        }
    }
  
    public static void main (String[] args) {
        StrStr st = new StrStr();
        String s1 = "sa";
        String s2 = "sssaltsa";
        System.out.println(st.strstr(s1, s2));
    }
}
