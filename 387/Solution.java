////////////////////////////////////Qestion//////////////////////////////////
/* Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1.
 * Examples:
 * s = "leetcode",    return 0.
 * s = "loveleetcode",    return 2.
 */

////////////////////////////////////Solution//////////////////////////////////
public class Solution {
	public int firstUniqChar(String s)
	{
	    int l = s.length();
	    for (char c : s.toCharArray())
	    {
	    	System.out.println(c);
	    	for (int i = s.indexOf(c) + 1; i < l; i++)
	    	{
	    		if (c == s.charAt(i)) break;
	    		else if (i == l - 1) return s.indexOf(c);
	    	}
	    }
	    return -1;
	}

	static public void main(String args[])
	{
		Solution q = new Solution();
		String s = "leetcode";
		System.out.println(q.firstUniqChar(s));
		String a = "loveleetcode";
		System.out.println(q.firstUniqChar(a));

	}
}